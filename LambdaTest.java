
public class LambdaTest{

  public static void main(String[] args){
   //Expresión lambda ==> representa un objeto de una interaz funcional
   FunctionTest ft=() -> System.out.println("Hola mundo :)");
   FunctionTest f=() -> System.out.println("soy vanessa :)");

   //ft.saludar();
LambdaTest objeto= new LambdaTest();
objeto.miMetodo(ft);
objeto.miMetodo(f);
}
public void miMetodo(FunctionTest parametro){
parametro.saludar();
}
}